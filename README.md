# openP23R

Die Projekte der Initiative *openP23R* implementieren die verschiedenen
Komponenten bespielhaft. Alle Komponenten der Initiative sowie weitere
Werkzeuge sind als Open Source Software verfügbar.

## P23R

Das Ziel des Projektes *P23R* ("Prozessdatenbeschleuniger") wird in der P23R-Rahmenarchitektur wie folgt beschrieben:

Die Grundlage des P23R-Prinzips ist die einheitliche Aufbereitung der Daten in Unternehmen zur Erfüllung ihrer Informations- und Meldepflichten gegenüber dem Staat. Ziel der Rahmenarchitektur ist es, eine Infrastruktur für die sichere, rechtskonforme und medienbruchfreie Durchführung zur Übermittlung von Informations- und Meldepflichten mit zukunftsweisenden Architekturbausteinen zu definieren.

Ein wesentlicher Aspekt bei der Gestaltung der sog. „P23R-Infrastruktur“ ist dabei der Rückgriff auf das Konzept von serviceorientierten Architekturen, der sich als Leitgedanke durch die Rahmenarchitektur zieht. Weiterhin soll die Rahmenarchitektur Aspekte des Datenschutzes und der Datensicherheit berücksichtigen, bzw. deren Umsetzung ermöglichen. Für die Kommunikation zwischen den Komponenten der P23R-Infrastruktur bzw. für die Kommunikation mit externen Komponenten werden verbindliche Schnittstellen definiert, die das Zusammenspiel von Bausteinen unterschiedlicher Hersteller ermöglichen.
Für die Aufbereitung der Unternehmensdaten und deren Übermittlung an den Adressaten sind im P23R-Prinzip mehrere Komponenten erforderlich, die nachfolgend als Bestandteile der Rahmenarchitektur mit ihren Funktionalitäten erläutert werden. Dieses sind (siehe auch Abbildung):

* Benachrichtigungsregeln und deren Verarbeitung im P23R-Prozessor
* Pivot-Datenmodell und Quelldatenkonnektor an der Schnittstelle zwischen dem IT-Fachsystem im Unternehmen und dem P23R-Prozessor im Unternehmen
* P23R-Leitstelle, die die Benachrichtigungsregeln im P23R-Depot und Zuständigkeitsinformationen in einem frei zugänglichen Quelldatenkonnektor bereitstellt
* Kommunikationskonnektor an der Schnittstelle zu den IT-Fachverfahren der Behörden
* Schnittstellen zur Verwaltung der Benachrichtigungsregeln
* sowie Schnittstellen zur Freigabe von Benachrichtigungen wie z. B. einem P23R-Client als Anwendungsoberfläche

![P23R Komponenten](p23rComponents.png)

Die Spezifikationen zum P23R sind veröffentlicht auf

* [P23R-Webseite](http://www.p23r.de "P23R Website")
* [P23R-Entwicklerportal](http://entwickler.p23r.de "P23R Entwicklerportal")

## Offizielle Komponenten der P23R-Infrastruktur

Der Quellcode der Komponenten ist in verschiedenen Repositories abgelegt.

### P23R Prozessor und Konnektoren

Implementation of a P23R processor, a generic source connector and a generic communication connector.

https://gitlab.com/openp23r/p23r-server

### P23R Client

Implementation of a generic P23R client to demonstrate the p23r solution.

https://gitlab.com/openp23r/p23r-client

### P23R Web Services

These are the specifications of the web services used by the various p23r components as described in the public p23r specifications.

https://gitlab.com/openp23r/p23r-core-webservices

### P23R Technische Benachrichtigungsregelsprache (T-BRS)

These are the data models of the p23r specification used by the notification rule language.

https://gitlab.com/openp23r/p23r-core-nrl-models

### P23R selection compiler

The compiler translates p23r selection expressions to executable code for the p23r processor or p23r source connector. The current version generates XQuery code for the public components of the openP23R initiative.

https://gitlab.com/openp23r/p23r-selection-compiler

## Zusätzliche Werkzeuge

Diese Komponenten und Werkzeuge sind nicht Teil der offiziellen Spezifikation des P23R.

### P23R model compiler

The p23r model describes a data model in a more simple way using our own DSL. The compiler translates a p23r model to a XML schema definition (XSD). P23R model is internally used in the project but is not part of the official P23R specifications.

https://gitlab.com/openp23r/p23r-model-compiler

## Legal issues

This project is part of the *openP23R* initiative.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS